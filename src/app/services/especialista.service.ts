import { HttpClient } from '@angular/common/http';
import { HOST } from './../shared/var.constant';
import { Subject } from 'rxjs';
import { especialista } from './../model/especialista';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EspecialistaService {
  especialistaCambio=new Subject<especialista[]>();
  mensajeCambio=new Subject<string>();
  url:string=`${HOST}/especialista`;
  constructor(private http:HttpClient) { }
  listar(){    
    return this.http.get<especialista[]>(this.url);

  }
  modificar(especialista:especialista){    
    console.log(this.url);
    console.log(especialista);
    return this.http.put(this.url,especialista);
  }
  registar(especialista:especialista){
    
    return this.http.post(this.url,especialista);
  }
  listarPorId(id:number){
    
    return this.http.get<especialista>(`${this.url}/${id}`);    
  }
  eliminar(id:number){    
    return this.http.delete(`${this.url}/${id}`);
  }
}
