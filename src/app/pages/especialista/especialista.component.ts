import { EspecialistaService } from './../../services/especialista.service';
import { especialista } from './../../model/especialista';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';


@Component({
  selector: 'app-especialista',
  templateUrl: './especialista.component.html',
  styleUrls: ['./especialista.component.css']
})
export class EspecialistaComponent implements OnInit {

  especialista:especialista;
  edicion:boolean=false;
  dataSource:MatTableDataSource<especialista>;
  displayedColumns=['id','nombreEspecialista','rutEspecialista','emailEspecialista','acciones'];
  @ViewChild(MatPaginator) paginator:MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(private snackBar:MatSnackBar,private especialistaService:EspecialistaService) { 

  }
  ngOnInit() {
    this.especialistaService.especialistaCambio.subscribe(data => {
      this.dataSource=new MatTableDataSource(data);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });
    this.especialistaService.mensajeCambio.subscribe(data => {
      this.snackBar.open(data,'Aviso',{duration:2000});
    });
    
    this.especialistaService.listar().subscribe(data=>{
      this.dataSource=new MatTableDataSource(data);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });
        
    }
  }


