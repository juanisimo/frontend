import { EspecialistaService } from './../../../services/especialista.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { especialista } from 'src/app/model/especialista';

@Component({
  selector: 'app-especialista-edicion',
  templateUrl: './especialista-edicion.component.html',
  styleUrls: ['./especialista-edicion.component.css']
})
export class EspecialistaEdicionComponent implements OnInit {

  form:FormGroup;
  id:number;
  especialista:especialista;
  edicion:boolean=false;
  constructor(private route:ActivatedRoute,private router:Router,private especialistaService:EspecialistaService) { 
    this.form=new FormGroup({
      'id': new FormControl(0),
      'descripcion':new FormControl(''),            
    }); 
  }
  initForm(){
    if(this.edicion){
      this.especialistaService.listarPorId(this.id).subscribe(data=>{
        this.form=new FormGroup({
          'id': new FormControl(data.id),
          'nombreEspecialista':new FormControl(data.nombreEspecialista),
          'rutEspecialista':new FormControl(data.rutEspecialista),
          'emailEspecialista':new FormControl(data.emailEspecialista),
        });
      })
        
    }
  }
  ngOnInit() {
    this.especialista=new especialista();
    this.route.params.subscribe((params: Params)=>{
      this.id=params['id'];
      this.edicion=params['id']!=null;
      this.initForm();      
    });
  }
  operar(){
    this.especialista.id=this.form.value['id'];
    this.especialista.nombreEspecialista=this.form.value['nombreEspecialista'];
    this.especialista.rutEspecialista=this.form.value['rutEspecialista'];
    this.especialista.emailEspecialista=this.form.value['emailEspecialista'];        
    if(this.edicion){

      this.especialistaService.modificar(this.especialista).subscribe(data=>
      {
        this.especialistaService.listar().subscribe(especialistas => {this.especialistaService.especialistaCambio.next(especialistas);       
        this.especialistaService.mensajeCambio.next("Se realizo el cambio de manera exitosa!");
      });
    });
    
    }
    else{
      this.especialistaService.registar(this.especialista).subscribe(data=>{
        this.especialistaService.listar().subscribe(especialistas => {this.especialistaService.especialistaCambio.next(especialistas);       
        this.especialistaService.mensajeCambio.next("Guardado exitoso!");
        });
      });
      
    }
    this.router.navigate(['especialista']);
  }


}
