import { PacienteService } from './../../../services/paciente.service';
import { Paciente } from './../../../model/paciente';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-paciente-edicion',
  templateUrl: './paciente-edicion.component.html',
  styleUrls: ['./paciente-edicion.component.css']
})
export class PacienteEdicionComponent implements OnInit {
  form:FormGroup;
  id:number;
  paciente:Paciente;
  edicion:boolean=false;
  constructor(private route:ActivatedRoute,private router:Router,private pacienteService:PacienteService) { 
    this.form=new FormGroup({
      'id': new FormControl(0),
      'nombrePaciente':new FormControl(''),
      'rutPaciente':new FormControl(''),
      'emailPaciente':new FormControl(''),
      'direccionPaciente':new FormControl(''),            
    }); 
  }
  initForm(){
    if(this.edicion){
      this.pacienteService.listarPorId(this.id).subscribe(data=>{
        this.form=new FormGroup({
          'id': new FormControl(data.id),
          'nombrePaciente':new FormControl(data.nombrePaciente),
          'rutPaciente':new FormControl(data.rutPaciente),
          'emailPaciente':new FormControl(data.emailPaciente),
          'direccionPaciente':new FormControl(data.direccionPaciente),
        });
      })
        
    }
  }
  ngOnInit() {
    this.paciente=new Paciente();
    this.route.params.subscribe((params: Params)=>{
      this.id=params['id'];
      this.edicion=params['id']!=null;
      this.initForm();      
    });
  }
  operar(){
    this.paciente.id=this.form.value['id'];
    this.paciente.nombrePaciente=this.form.value['nombrePaciente'];
    this.paciente.rutPaciente=this.form.value['rutPaciente'];
    this.paciente.emailPaciente=this.form.value['emailPaciente'];
    this.paciente.direccionPaciente=this.form.value['direccionPaciente'];        
    if(this.edicion){

      this.pacienteService.modificar(this.paciente).subscribe(data=>
      {
        this.pacienteService.listar().subscribe(pacientes => {this.pacienteService.pacienteCambio.next(pacientes);       
        this.pacienteService.mensajeCambio.next("Se realizo el cambio de manera exitosa!");
      });
    });
    
    }
    else{
      this.pacienteService.registar(this.paciente).subscribe(data=>{
        this.pacienteService.listar().subscribe(pacientes => {this.pacienteService.pacienteCambio.next(pacientes);       
        this.pacienteService.mensajeCambio.next("Guardado exitoso!");
        });
      });
      
    }
    this.router.navigate(['pacientes']);
  }

}
