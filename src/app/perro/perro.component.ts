import { Component, OnInit, EventEmitter, Output, Input} from '@angular/core';

@Component({
  selector: 'app-perro',
  templateUrl: './perro.component.html',
  styleUrls: ['./perro.component.css']
})
export class PerroComponent implements OnInit {
  @Output()
  emisor=new EventEmitter<string>();
  @Input()
  nombre:string;
  constructor() { }

  ngOnInit() {
  }

ladrar(){
  this.emisor.emit("guau!");
}
}
